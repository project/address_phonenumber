---SUMMARY---

Address Phonenumber Module is intend to extend the Address module and provide
the Phone number field.

For a full description visit project page:
https://www.drupal.org/project/address_phonenumber

Bug reports, feature suggestions and latest developments:
https://www.drupal.org/project/issues/address_phonenumber

---REQUIREMENTS---

* A clean Drupal 8 installation.
* Address module.


---INSTALLATION---

[1] Install the Drupal 8 and Address module.
[2] Download Address Phonenumber module
[3] Run the composer update command because we need
   "giggsey/libphonenumber-for-php" library for phonenumber verification.
[4] Enable the address Phonenumber module.
[5] Go to config -> people -> user profiles and click on to Profile types tab.
   * There is a default profile type Customer is present so click on manage
     fields.
   * Now you can add new field "Address with Phonenumber".
   * Now Go to Manage form display and keep "Address with Phonenumber" field
     enabled and other Address field disabled.
   * Now Go to Manage display and keep "Address with Phonenumber" field
     enabled and other Address field disabled.
